/*
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.1 Copyright (C) Paul Johnston 1999 - 2002.
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
var b64pad  = ""; /* base-64 pad character. "=" for strict RFC compliance   */
var chrsz   = 8;  /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
function hex_md5(s){ return binl2hex(core_md5(str2binl(s), s.length * chrsz));}
function b64_md5(s){ return binl2b64(core_md5(str2binl(s), s.length * chrsz));}
function str_md5(s){ return binl2str(core_md5(str2binl(s), s.length * chrsz));}
function hex_hmac_md5(key, data) { return binl2hex(core_hmac_md5(key, data)); }
function b64_hmac_md5(key, data) { return binl2b64(core_hmac_md5(key, data)); }
function str_hmac_md5(key, data) { return binl2str(core_hmac_md5(key, data)); }

/*
 * Perform a simple self-test to see if the VM is working
 */
function md5_vm_test()
{
  return hex_md5("abc") == "900150983cd24fb0d6963f7d28e17f72";
}

/*
 * Calculate the MD5 of an array of little-endian words, and a bit length
 */
function core_md5(x, len)
{
  /* append padding */
  x[len >> 5] |= 0x80 << ((len) % 32);
  x[(((len + 64) >>> 9) << 4) + 14] = len;

  var a =  1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d =  271733878;

  for(var i = 0; i < x.length; i += 16)
  {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;

    a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
    d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
    c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
    b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
    a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
    d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
    c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
    b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
    a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
    d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
    c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
    b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
    a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
    d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
    c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
    b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);

    a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
    d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
    c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
    b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
    a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
    d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
    c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
    b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
    a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
    d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
    c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
    b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
    a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
    d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
    c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
    b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);

    a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
    d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
    c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
    b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
    a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
    d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
    c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
    b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
    a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
    d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
    c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
    b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
    a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
    d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
    c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
    b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);

    a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
    d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
    c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
    b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
    a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
    d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
    c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
    b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
    a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
    d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
    c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
    b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
    a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
    d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
    c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
    b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);

    a = safe_add(a, olda);
    b = safe_add(b, oldb);
    c = safe_add(c, oldc);
    d = safe_add(d, oldd);
  }
  return Array(a, b, c, d);

}

/*
 * These functions implement the four basic operations the algorithm uses.
 */
function md5_cmn(q, a, b, x, s, t)
{
  return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
}
function md5_ff(a, b, c, d, x, s, t)
{
  return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
}
function md5_gg(a, b, c, d, x, s, t)
{
  return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
}
function md5_hh(a, b, c, d, x, s, t)
{
  return md5_cmn(b ^ c ^ d, a, b, x, s, t);
}
function md5_ii(a, b, c, d, x, s, t)
{
  return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
}

/*
 * Calculate the HMAC-MD5, of a key and some data
 */
function core_hmac_md5(key, data)
{
  var bkey = str2binl(key);
  if(bkey.length > 16) bkey = core_md5(bkey, key.length * chrsz);

  var ipad = Array(16), opad = Array(16);
  for(var i = 0; i < 16; i++)
  {
    ipad[i] = bkey[i] ^ 0x36363636;
    opad[i] = bkey[i] ^ 0x5C5C5C5C;
  }

  var hash = core_md5(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
  return core_md5(opad.concat(hash), 512 + 128);
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bit_rol(num, cnt)
{
  return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * Convert a string to an array of little-endian words
 * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
 */
function str2binl(str)
{
  var bin = Array();
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < str.length * chrsz; i += chrsz)
    bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);
  return bin;
}

/*
 * Convert an array of little-endian words to a string
 */
function binl2str(bin)
{
  var str = "";
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < bin.length * 32; i += chrsz)
    str += String.fromCharCode((bin[i>>5] >>> (i % 32)) & mask);
  return str;
}

/*
 * Convert an array of little-endian words to a hex string.
 */
function binl2hex(binarray)
{
  var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i++)
  {
    str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) +
           hex_tab.charAt((binarray[i>>2] >> ((i%4)*8  )) & 0xF);
  }
  return str;
}

/*
 * Convert an array of little-endian words to a base-64 string
 */
function binl2b64(binarray)
{
  var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i += 3)
  {
    var triplet = (((binarray[i   >> 2] >> 8 * ( i   %4)) & 0xFF) << 16)
                | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 )
                |  ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
    for(var j = 0; j < 4; j++)
    {
      if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
      else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
    }
  }
  return str;
}

// Generated by CoffeeScript 1.3.3
(function() {
    var SwaggerApi, SwaggerOperation, SwaggerRequest, SwaggerResource,
    __bind = function(fn, me){
        return function(){
            return fn.apply(me, arguments);
        };    
};

SwaggerApi = (function() {

    SwaggerApi.prototype.discoveryUrl = "http://api.wordnik.com/v4/resources.json";

    SwaggerApi.prototype.debug = false;

    SwaggerApi.prototype.api_key = null;

    SwaggerApi.prototype.basePath = null;

    function SwaggerApi(options) {
        if (options == null) {
            options = {};
        }
        if (options.discoveryUrl != null) {
            this.discoveryUrl = options.discoveryUrl;
        }
        if (options.debug != null) {
            this.debug = options.debug;
        }
        this.apiKeyName = options.apiKeyName != null ? options.apiKeyName : 'api_key';
        if (options.apiKey != null) {
            this.api_key = options.apiKey;
        }
        if (options.api_key != null) {
            this.api_key = options.api_key;
        }
        if (options.verbose != null) {
            this.verbose = options.verbose;
        }
        this.supportHeaderParams = options.supportHeaderParams != null ? options.supportHeaderParams : false;
        this.supportedSubmitMethods = options.supportedSubmitMethods != null ? options.supportedSubmitMethods : ['get'];
        if (options.success != null) {
            this.success = options.success;
        }
        this.failure = options.failure != null ? options.failure : function() {};
        this.progress = options.progress != null ? options.progress : function() {};
        this.discoveryUrl = this.suffixApiKey(this.discoveryUrl);
        if (options.success != null) {
            this.build(options);
        }
    }

    SwaggerApi.prototype.build = function(options) {
        var _this = this;
        this.progress('fetching resource list: ' + this.discoveryUrl);
        return jQuery.getJSON(this.discoveryUrl, function(response) {
            var res, resource, _i, _j, _len, _len1, _ref, _ref1;
            //添加 basePath 选项
            response.basePath=options.basePath?options.basePath: response.basePath;
            if ((response.basePath != null) && jQuery.trim(response.basePath).length > 0) {
                _this.basePath = response.basePath;
                if (_this.basePath.match(/^HTTP/i) == null) {
                    _this.fail("discoveryUrl basePath must be a URL.");
                }
                _this.basePath = _this.basePath.replace(/\/$/, '');
            } else {
                _this.basePath = _this.discoveryUrl.substring(0, _this.discoveryUrl.lastIndexOf('/'));
                log('derived basepath from discoveryUrl as ' + _this.basePath);
            }
            _this.resources = {};
            _this.resourcesArray = [];
            if (response.resourcePath != null) {
                _this.resourcePath = response.resourcePath;
                res = null;
                _ref = response.apis;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    resource = _ref[_i];
                    if (res === null) {
                        res = new SwaggerResource(resource, _this,options);
                    } else {
                        res.addOperations(resource.path, resource.operations);
                    }
                }
                if (res != null) {
                    _this.resources[res.name] = res;
                    _this.resourcesArray.push(res);
                    res.ready = true;
                    _this.selfReflect();
                }
            } else {
                _ref1 = response.apis;
                for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                    resource = _ref1[_j];
                    res = new SwaggerResource(resource, _this,options);
                    _this.resources[res.name] = res;
                    _this.resourcesArray.push(res);
                }
            }
            return _this;
        }).error(function(error) {
            return _this.fail(error.status + ' : ' + error.statusText + ' ' + _this.discoveryUrl);
        });
    };

    SwaggerApi.prototype.selfReflect = function() {
        var resource, resource_name, _ref;
        if (this.resources == null) {
            return false;
        }
        _ref = this.resources;
        for (resource_name in _ref) {
            resource = _ref[resource_name];
            if (resource.ready == null) {
                return false;
            }
        }
        this.ready = true;
        if (this.success != null) {
            return this.success();
        }
    };

    SwaggerApi.prototype.fail = function(message) {
        this.failure(message);
        throw message;
    };

    SwaggerApi.prototype.suffixApiKey = function(url) {
        var sep;
        var randTimestamp=new Date().getTime();
        if ((this.api_key != null) && jQuery.trim(this.api_key).length > 0 && (url != null)) {
            sep = url.indexOf('?') > 0 ? '&' : '?';
            return url + sep + this.apiKeyName + '=' + this.api_key+'&__rnd='+randTimestamp;
        } else {
            return url+'?__rnd='+randTimestamp;
        }
    };

    SwaggerApi.prototype.help = function() {
        var operation, operation_name, parameter, resource, resource_name, _i, _len, _ref, _ref1, _ref2;
        _ref = this.resources;
        for (resource_name in _ref) {
            resource = _ref[resource_name];
            console.log(resource_name);
            _ref1 = resource.operations;
            for (operation_name in _ref1) {
                operation = _ref1[operation_name];
                console.log("  " + operation.nickname);
                _ref2 = operation.parameters;
                for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
                    parameter = _ref2[_i];
                    console.log("    " + parameter.name + (parameter.required ? ' (required)' : '') + " - " + parameter.description);
                }
            }
        }
        return this;
    };

    return SwaggerApi;

})();

    SwaggerResource = (function() {

        function SwaggerResource(resourceObj, api,options) {
            var parts,
            _this = this;
            this.api = api;
            this.path = this.api.resourcePath != null ? this.api.resourcePath : resourceObj.path;
            this.description = resourceObj.description;
            parts = this.path.split("/");
            this.name = parts[parts.length - 1].replace('.{format}', '');
            this.basePath = this.api.basePath;
            this.operations = {};
            this.operationsArray = [];
            if ((resourceObj.operations != null) && (this.api.resourcePath != null)) {
                this.api.progress('reading resource ' + this.name + ' operations');
                this.addOperations(resourceObj.path, resourceObj.operations);
                this.api[this.name] = this;
            } else { 
                if (this.path == null) {
                    this.api.fail("SwaggerResources must have a path.");
                }
                this.url = this.api.suffixApiKey(this.api.basePath + this.path.replace('{format}', 'json'));
                this.api.progress('fetching resource ' + this.name + ': ' + this.url);
                jQuery.getJSON(this.url, function(response) { 
                    var endpoint, _i, _len, _ref; 
                    //定制获取资源的api base path
                    response.basePath=response.basePath?response.basePath:options.apiPath;
                    if ((response.basePath != null) && jQuery.trim(response.basePath).length > 0) {
                        _this.basePath = response.basePath;
                        _this.basePath = _this.basePath.replace(/\/$/, '');
                    }
                    if (response.apis) {
                        _ref = response.apis;
                        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                            endpoint = _ref[_i];
                            _this.addOperations(endpoint.path, endpoint.operations);
                        }
                    }
                    _this.api[_this.name] = _this;
                    _this.ready = true;
                    return _this.api.selfReflect();
                }).error(function(error) {
                    return _this.fail(error.status + ' : ' + error.statusText + ' ' + _this.url);
                });
            }
        }

        SwaggerResource.prototype.addOperations = function(resource_path, ops) {
            var o, op, _i, _len, _results;
            if (ops) {
                _results = [];
                for (_i = 0, _len = ops.length; _i < _len; _i++) {
                    o = ops[_i]; 
                    op = new SwaggerOperation(o.nickname, resource_path, o.httpMethod, o.parameters, o.summary, o.notes, this,o.description,
                        o.updateTime,o.reviser,o.revisionDesc,o.fieldDesc,o.exampleJSON,o.apiStatus,o.newset);
                    this.operations[op.nickname] = op;
                    _results.push(this.operationsArray.push(op));
                }
                return _results;
            }
        };

        SwaggerResource.prototype.help = function() {
            var operation, operation_name, parameter, _i, _len, _ref, _ref1;
            _ref = this.operations;
            for (operation_name in _ref) {
                operation = _ref[operation_name];
                console.log("  " + operation.nickname);
                _ref1 = operation.parameters;
                for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
                    parameter = _ref1[_i];
                    console.log("    " + parameter.name + (parameter.required ? ' (required)' : '') + " - " + parameter.description);
                }
            }
            return this;
        };

        return SwaggerResource;

    })();

    SwaggerOperation = (function() {
        function SwaggerOperation(nickname, path, httpMethod, parameters, summary, notes,
        resource,description,updateTime,reviser,revisionDesc,fieldDesc,exampleJSON,apiStatus,newset) {
            var parameter, v, _i, _j, _len, _len1, _ref, _ref1,
            _this = this;
            this.nickname = path.replace(/[\/\{\}]/ig,'');
            this.path = path;
            this.httpMethod = httpMethod;
            this.parameters = parameters != null ? parameters : [];
            this.summary = summary;
            this.notes = notes;
            this.resource = resource;
            this.description=description;
            this.updateTime=updateTime;
            this.reviser=reviser;
            this.revisionDesc=revisionDesc;
            this.fieldDesc=fieldDesc;
            this.exampleJSON=exampleJSON;
            this.apiStatus=apiStatus;
            this.newset=newset;
            this["do"] = __bind(this["do"], this);

            if (this.nickname == null) {
                this.resource.api.fail("SwaggerOperations must have a nickname.");
            }
            if (this.path == null) {
                this.resource.api.fail("SwaggerOperation " + nickname + " is missing path.");
            }
            if (this.httpMethod == null) {
                this.resource.api.fail("SwaggerOperation " + nickname + " is missing httpMethod.");
            }
            this.path = this.path.replace('{format}', 'json');
            this.httpMethod = this.httpMethod.toLowerCase();
            this.isGetMethod = this.httpMethod === "get";
            this.resourceName = this.resource.name;
            _ref = this.parameters;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                parameter = _ref[_i];
                parameter.name = parameter.name || parameter.dataType;

                if (parameter.allowableValues != null) {
                    if (parameter.allowableValues.valueType === "RANGE") {
                        parameter.isRange = true;
                    } else {
                        parameter.isList = true;
                    }
                    if (parameter.allowableValues.values != null) {
                        parameter.allowableValues.descriptiveValues = [];
                        _ref1 = parameter.allowableValues.values;
                        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                            v = _ref1[_j];
                            if ((parameter.defaultValue != null) && parameter.defaultValue === v) {
                                parameter.allowableValues.descriptiveValues.push({
                                    value: v,
                                    isDefault: true
                                });
                            } else {
                                parameter.allowableValues.descriptiveValues.push({
                                    value: v,
                                    isDefault: false
                                });
                            }
                        }
                    }
                }
            }
            this.resource[this.nickname] = function(args, callback, error) {
                return _this["do"](args, callback, error);
            };
        }

        SwaggerOperation.prototype["do"] = function(args, callback, error) {
            var body, headers;
            if (args == null) {
                args = {};
            }
            if ((typeof args) === "function") {
                error = callback;
                callback = args;
                args = {};
            }
            if (error == null) {
                error = function(xhr, textStatus, error) {
                    return console.log(xhr, textStatus, error);
                };
            }
            if (callback == null) {
                callback = function(data) {
                    return console.log(data);
                };
            }
            if (args.headers != null) {
                headers = args.headers;
                delete args.headers;
            }
            alert(args.body)
            if (args.body != null) {
                body = args.body;
                delete args.body;
            }
            return new SwaggerRequest(this.httpMethod, this.urlify(args), headers, body, callback, error, this);
        };

        SwaggerOperation.prototype.pathJson = function() {
            return this.path.replace("{format}", "json");
        };

        SwaggerOperation.prototype.pathXml = function() {
            return this.path.replace("{format}", "xml");
        };

        SwaggerOperation.prototype.urlify = function(args, includeApiKey) {
            var param, queryParams, url, _i, _len, _ref;
            if (includeApiKey == null) {
                includeApiKey = true;
            }
            url = this.resource.basePath + this.pathJson();
            url=url.replace(/\/*$/,'')+'/';
            _ref = this.parameters;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                param = _ref[_i];
                if (param.paramType === 'path') {
                    if(_i==0)url+='?';
                    if (args[param.name]) { 
                        var regex=new RegExp('/\{'+param.name+'\}/');
                        if(url.match(regex)){
                            url = url.replace("{" + param.name + "}", encodeURIComponent(args[param.name]));
                        }else{
                            url=url+param.name+'='+args[param.name]+  '&';
                        }
                        //修改为后缀加入参数
                        // url=url+param.name+'/'+args[param.name]+'/';
                        delete args[param.name];
                    } else {  
                    //            throw "" + param.name + " is a required path param.";
                    }
                }
            }
            url=url.replace(/[\/\?\&]*$/,'');

            if (includeApiKey && (this.resource.api.api_key != null) && this.resource.api.api_key.length > 0) {
                args[this.apiKeyName] = this.resource.api.api_key;
            }
            if (this.supportHeaderParams()) {
                queryParams = jQuery.param(this.getQueryParams(args));
            } else {
                queryParams = jQuery.param(this.getQueryAndHeaderParams(args));
            }
            if ((queryParams != null) && queryParams.length > 0) {
                var regex=/\?/;
                if(!regex.test(url)){
                    url += "?" + queryParams;
                }else{
                    url +=  queryParams;
                }
            }
            return url;
        };

        SwaggerOperation.prototype.supportHeaderParams = function() {
            return this.resource.api.supportHeaderParams;
        };

        SwaggerOperation.prototype.supportedSubmitMethods = function() {
            return this.resource.api.supportedSubmitMethods;
        };

        SwaggerOperation.prototype.getQueryAndHeaderParams = function(args, includeApiKey) {
            if (includeApiKey == null) {
                includeApiKey = true;
            }
            return this.getMatchingParams(['query', 'header'], args, includeApiKey);
        };

        SwaggerOperation.prototype.getQueryParams = function(args, includeApiKey) {
            if (includeApiKey == null) {
                includeApiKey = true;
            }
            return this.getMatchingParams(['query'], args, includeApiKey);
        };

        SwaggerOperation.prototype.getHeaderParams = function(args, includeApiKey) {
            if (includeApiKey == null) {
                includeApiKey = true;
            }
            return this.getMatchingParams(['header'], args, includeApiKey);
        };

        SwaggerOperation.prototype.getMatchingParams = function(paramTypes, args, includeApiKey) {
            var matchingParams, param, _i, _len, _ref;
            matchingParams = {};
            _ref = this.parameters;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                param = _ref[_i];
                if ((jQuery.inArray(param.paramType, paramTypes) >= 0) && args[param.name]) {
                    matchingParams[param.name] = args[param.name];
                }
            }
            // var currentTime=new Date().getTime();
            // var currentTime=Date.parse(new Date())/1000; 
            // var requid=$('input[name="requid"]').val();
            // if (requid && includeApiKey && (this.resource.api.api_key != null) && this.resource.api.api_key.length > 0) {
            //     // matchingParams[this.resource.api.apiKeyName] = this.resource.api.api_key;
            //     matchingParams['requid'] =requid;
            //     matchingParams['reqtime'] =currentTime;
            //     matchingParams['key'] =hex_md5(matchingParams['reqtime']+this.resource.api.api_key);
            // }else{
            //     matchingParams['reqtime'] =currentTime;
            //     matchingParams['key'] =  hex_md5(matchingParams['reqtime']+'collection666');
            // }
            return matchingParams;
        };

        SwaggerOperation.prototype.help = function() {
            var parameter, _i, _len, _ref;
            _ref = this.parameters;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                parameter = _ref[_i];
                console.log("    " + parameter.name + (parameter.required ? ' (required)' : '') + " - " + parameter.description);
            }
            return this;
        };

        return SwaggerOperation;

    })();

    SwaggerRequest = (function() {

        function SwaggerRequest(type, url, headers, body, successCallback, errorCallback, operation) { 
            var obj,
            _this = this;
            this.type = type;
            this.url = url;
            this.headers = headers;
            this.body = body;
            this.successCallback = successCallback;
            this.errorCallback = errorCallback;
            this.operation = operation;
            if (this.type == null) {
                throw "SwaggerRequest type is required (get/post/put/delete).";
            }
            if (this.url == null) {
                throw "SwaggerRequest url is required.";
            }
            if (this.successCallback == null) {
                throw "SwaggerRequest successCallback is required.";
            }
            if (this.errorCallback == null) {
                throw "SwaggerRequest error callback is required.";
            }
            if (this.operation == null) {
                throw "SwaggerRequest operation is required.";
            }
            if (this.operation.resource.api.verbose) {
                console.log(this.asCurl());
            }
            this.headers || (this.headers = {});
            if (this.operation.resource.api.api_key != null) {
                this.headers[this.apiKeyName] = this.operation.resource.api.api_key;
            }
            if (this.headers.mock == null) {
                obj = {
                    type: this.type,
                    url: this.url,
                    data: JSON.stringify(this.body),
                    dataType: 'json',
                    error: function(xhr, textStatus, error) {
                        return _this.errorCallback(xhr, textStatus, error);
                    },
                    success: function(data) { 
                        return _this.successCallback(data);
                    }
                };
                if (obj.type.toLowerCase() === "post" || obj.type.toLowerCase() === "put") {
                    obj.contentType = "application/json";
                }
                jQuery.ajax(obj);
            }
        }

        SwaggerRequest.prototype.asCurl = function() {
            var header_args, k, v;
            header_args = (function() {
                var _ref, _results;
                _ref = this.headers;
                _results = [];
                for (k in _ref) {
                    v = _ref[k];
                    _results.push("--header \"" + k + ": " + v + "\"");
                }
                return _results;
            }).call(this);
            return "curl " + (header_args.join(" ")) + " " + this.url;
        };

        return SwaggerRequest;

    })();

    window.SwaggerApi = SwaggerApi;

    window.SwaggerResource = SwaggerResource;

    window.SwaggerOperation = SwaggerOperation;

    window.SwaggerRequest = SwaggerRequest;

}).call(this);

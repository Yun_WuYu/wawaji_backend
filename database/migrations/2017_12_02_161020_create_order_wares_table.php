<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderWaresTable extends Migration
{
    private $table = 'order_wares';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->comment('订单id');
            $table->unsignedInteger('ware_id')->comment('商品id');
            $table->string('name')->default('')->comment('商品名称');
            $table->unsignedInteger('num')->comment('数量');
            $table->string('image')->default('')->comment('商品图片');
            
            $table->index('order_id');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '订单商品表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderExtensionsTable extends Migration
{
    private $table = 'order_extensions';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('province_id')->default(0)->comment('省份id');
            $table->unsignedInteger('city_id')->default(0)->comment('城市id');
            $table->unsignedInteger('district_id')->default(0)->comment('城区id');
            $table->string('address')->default('')->comment('地址');
            $table->string('consignee', 20)->default('')->comment('收货人');
            $table->string('phone', 11)->default('')->comment('收货人手机号');
            $table->string('shipping_no', 20)->default('')->comment('物流单号');
            $table->string('shipping_name', 50)->default('')->comment('快递公司');
            $table->string('shipping_com', 50)->default('')->comment('物流公司标识');
            $table->timestamp('shipped_at')->nullable()->comment('发货时间');
            
            $table->primary('order_id');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '订单扩展表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    private $table = 'coins';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('coin')->comment('游戏币个数');
            $table->unsignedInteger('coin_award')->default(0)->comment('赠送游戏币个数');
            $table->unsignedDecimal('money')->comment('支付金额');
            $table->tinyInteger('status')->default(1)->comment('状态：-1为不可用，1为可用');
            $table->integer('stick_time')->default(0)->comment('置顶时间：0未置顶，其他值则已置顶');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `{$this->table}` COMMENT '游戏币列表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}

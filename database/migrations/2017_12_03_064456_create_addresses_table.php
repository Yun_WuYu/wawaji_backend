<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    private $table = 'addresses';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->unsignedInteger('province_id')->comment('省份id');
            $table->unsignedInteger('city_id')->comment('城市id');
            $table->unsignedInteger('district_id')->comment('城区id');
            $table->string('address', 100)->comment('地址');
            $table->string('consignee', 20)->comment('收货人');
            $table->string('phone', 11)->comment('收货人手机号');
            $table->tinyInteger('is_default')->default(0)->comment('是否为默认地址：0为否，1为是');
            $table->timestamps();
            
            $table->index('user_id');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '用户地址表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinRecordsTable extends Migration
{
    private $table = 'coin_records';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->integer('coin')->comment('游戏币个数（正负值）');
            $table->string('recordable_type', 50)->comment('记录项类型');
            $table->unsignedInteger('recordable_id')->comment('记录项实体id');
            $table->timestamp('created_at')->nullable();
            
            $table->index('user_id');
            $table->index(['recordable_type', 'recordable_id']);
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '游戏币记录表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

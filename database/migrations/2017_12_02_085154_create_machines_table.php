<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinesTable extends Migration
{
    private $table = 'machines';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 40)->default('')->comment('设备名称');
            $table->string('ware_name')->default('')->comment('商品名称');
            $table->string('device_sn', 20)->comment('设备编号');
            $table->string('device_id', 40)->comment('设备ID');
            $table->string('room_id', 50)->default('')->comment('房间号');
            $table->tinyInteger('device_status')->default(0)->comment('设备状态：0为空闲，1为游戏中，2为故障');
            $table->tinyInteger('game_mode')->default(1)->comment('游戏模式：1为普通模式，2为无限模式');
            $table->unsignedInteger('coin')->default(1)->comment('1次消耗游戏币个数');
            $table->string('ware_image')->default('')->comment('商品图片');
            $table->string('detail_image')->default('')->comment('详情图片');
            $table->tinyInteger('is_display')->default(1)->comment('显示：0为隐藏，1为显示');
            $table->integer('stick_time')->default(0)->comment('置顶时间：0未置顶，其他值则已置顶');
            $table->unsignedInteger('game_record_id')->default(0)->comment('最近一次游戏记录id');
            $table->unsignedInteger('latest_user_id')->default(0)->comment('最近一次的玩家用户id');
            $table->timestamp('played_at')->nullable()->comment('最近一次游戏开始时间');
            $table->timestamps();
            
            $table->unique('device_id');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '游戏机表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

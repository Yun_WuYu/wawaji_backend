<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    private $table = 'payments';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->char('trade_no', 32)->comment('交易单号');
            $table->string('status', 10)->comment('状态：unpaid, paid');
            $table->unsignedInteger('coin')->default(0)->comment('当次支付得到的游戏币个数（购买的+支付赠送的）');
            $table->unsignedDecimal('money')->comment('支付金额');
            $table->timestamp('paid_at')->nullable()->comment('支付时间');
            $table->string('payable_type', 50)->comment('记录项类型');
            $table->unsignedInteger('payable_id')->comment('记录项实体id');
            $table->string('source', 20)->default('')->comment('来源');
            $table->timestamps();

            $table->index('user_id');
            $table->unique('trade_no');
            $table->index(['payable_type', 'payable_id']);
        });

        DB::statement("ALTER TABLE `{$this->table}` COMMENT '支付表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

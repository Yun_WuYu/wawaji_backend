<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThirdPartyUsersTable extends Migration
{
    private $table = 'third_party_users';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->tinyInteger('type')->comment('第三方登录类型');
            $table->string('openid', 32)->comment('第三方openid');
            $table->timestamp('created_at')->nullable();
            
            $table->index('user_id');
            $table->unique(['type', 'openid']);
        });

        DB::statement("ALTER TABLE `{$this->table}` COMMENT '第三方用户表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

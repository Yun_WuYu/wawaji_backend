<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    private $table = 'orders';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->string('order_sn', 20)->comment('订单编号');
            $table->tinyInteger('send_type')->comment('发货方式：1为自行发货，2为供应商发货');
            $table->integer('status')->comment('状态：100为待发货，200为已发货，1000为完成');
            $table->unsignedInteger('out_order_id')->default(0)->comment('外部的订单id');
            $table->timestamp('confirmed_at')->nullable()->comment('确认收货时间');
            $table->softDeletes();
            $table->timestamps();

            $table->index('user_id');
            $table->index('out_order_id');
            $table->unique('order_sn');
        });

        DB::statement("ALTER TABLE `{$this->table}` COMMENT '订单表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

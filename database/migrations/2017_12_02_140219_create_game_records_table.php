<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameRecordsTable extends Migration
{
    private $table = 'game_records';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->unsignedInteger('machine_id')->comment('游戏机id');
            $table->unsignedInteger('log_id')->default(0)->comment('操作记录id');
            $table->unsignedInteger('coin')->default(0)->comment('耗费的游戏币个数');
            $table->tinyInteger('operate_result')->default(-1)->comment('操作结果：-1为未有结果，1为成功，2为失败');
            $table->unsignedInteger('ware_id')->default(0)->comment('商品id');
            $table->string('ware_name')->default('')->comment('商品名称');
            $table->string('ware_image')->default('')->comment('商品图片');
            $table->tinyInteger('ware_status')->default(0)->comment('商品状态：0为没有，1为待领取，2为已领取');
            $table->unsignedInteger('order_id')->default(0)->comment('订单id');
            $table->timestamp('ended_at')->nullable()->comment('游戏结束时间');
            $table->timestamps();
            
            $table->index('user_id');
            $table->unique('log_id');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '游戏记录表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

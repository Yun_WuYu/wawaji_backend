<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{
    private $table = 'regions';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->default(0)->comment('上级id');
            $table->string('name', 30)->comment('名称');
            $table->tinyInteger('level')->comment('行政区级别：1为省，2为市，3为区');
            
            $table->index('parent_id');
            $table->index('level');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '行政区表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    private $table = 'users';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname', 50)->default('')->comment('昵称');
            $table->tinyInteger('gender')->default(0)->comment('性别：0为未知，1为男，2为女');
            $table->string('avatar')->default('')->comment('头像');
            $table->integer('coin')->default(0)->comment('剩余游戏币个数');
            $table->integer('coin_buy')->default(0)->comment('总的购买的游戏币个数');
            $table->unsignedDecimal('cost_money', 11, 2)->default(0.00)->comment('消费总金额');
            $table->string('unionid', 32)->nullable()->comment('微信unionid');
            $table->string('api_token', 60);
            $table->timestamps();
            
            $table->unique('unionid');
            $table->unique('api_token');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '用户表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

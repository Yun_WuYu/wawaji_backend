<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    private $table = 'banners';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->default('')->comment('图片路径');
            $table->tinyInteger('is_display')->default(1)->comment('显示：0为隐藏，1为显示');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `{$this->table}` COMMENT '轮播图表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

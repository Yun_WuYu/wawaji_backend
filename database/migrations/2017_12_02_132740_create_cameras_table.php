<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamerasTable extends Migration
{
    private $table = 'cameras';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('machine_id')->comment('游戏机id');
            $table->string('type', 10)->comment('类型：front为正面，side为侧面');
            $table->string('live_openid', 50)->default('')->comment('直播openid');
            $table->string('rtmp_address_main')->default('')->comment('操作者的rtmp流地址');
            $table->string('rtmp_address')->default('')->comment('rtmp流地址');
            $table->string('hls_address')->default('')->comment('hls流地址');
            
            $table->index('machine_id');
        });
        
        DB::statement("ALTER TABLE `{$this->table}` COMMENT '摄像头表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

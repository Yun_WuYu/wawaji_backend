<?php
$docdir = '/apidoc/';
?>
<html>
    <head>
        <title><?php echo $title; ?> - API Documentation</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href='<?=$docdir . 'css/screen.css';?>' media='screen' rel='stylesheet' type='text/css'/>
        <script src='<?=$docdir . 'lib/jquery.min.js';?>' type='text/javascript'></script>
        <script src='<?=$docdir . 'lib/jquery.slideto.min.js';?>' type='text/javascript'></script>
        <script src='<?=$docdir . 'lib/jquery.wiggle.min.js';?>' type='text/javascript'></script>
        <script src='<?=$docdir . 'lib/jquery.ba-bbq.min.js';?>' type='text/javascript'></script>
        <script src='<?=$docdir . 'lib/handlebars.runtime-1.0.0.beta.6.js';?>' type='text/javascript'></script>
        <script src='<?=$docdir . 'lib/underscore-min.js';?>' type='text/javascript'></script>
        <script src='<?=$docdir . 'lib/backbone-min.js';?>' type='text/javascript'></script>
        <script src='<?=$docdir . 'lib/swagger.js';?>?__rnd=20130917' type='text/javascript'></script>
        <script src='<?=$docdir . 'swagger-ui.js';?>' type='text/javascript'></script>
        <style type="text/css">
            .swagger-ui-wrap {
                max-width: 1000px;
                margin-left: auto;
                margin-right: auto;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                if ($.browser.msie && $.browser.version <= '8.0') {
                    alert('你正在使用IE版本的浏览器，将导致文档不可用，建议使用其他浏览器（Sarafi/火狐/Chrome） Screw IE！');
                    location.href = "/";
                }
                function get_hostname(url) {
                    var m = url.match(/^(http|https):\/\/[^/]+/);
                    return m ? m[0] : null;
                }
                var hosturl = location.href;
                hosturl = get_hostname(hosturl);
                hosturl = hosturl.replace(/\/$/g, '');

                window.swaggerUi = new SwaggerUi({
                    discoveryUrl: hosturl + "<?=$docdir;?><?=$resourcePath;?>",
                    apiKeyName: "token",
                    apiKey: "",
                    basePath: hosturl + "<?=$docdir;?>rs/",
                    apiPath: hosturl ,
                    dom_id: "swagger-ui-container",
                    supportHeaderParams: false,
                    supportedSubmitMethods: ['get', 'post', 'put','update','delete']
                });

                window.swaggerUi.load(function() {
                });

                var getTimeInteger = function(timeString) {
                    return  parseInt(timeString.replace(/[-\/]/g, ''));
                }
            });

        </script>
    </head>
    <body>
        <div id="header">
            <div class="swagger-ui-wrap">
                <a id="logo" href="<?php echo $_SERVER['REQUEST_URI']; ?>"><?php echo $title; ?> API</a>

                <form id='api_selector'>
                    <!--            <div class='input icon-btn'>
                                    <img id="show-pet-store-icon" src="images/pet_store_api.png" title="Show Swagger Petstore Example Apis">
                                </div>
                                <div class='input icon-btn'>
                                    <img id="show-wordnik-dev-icon" src="images/wordnik_api.png" title="Show Wordnik Developer Apis">
                                </div>-->
                    <div class='input'><input placeholder="http://example.com/api" id="input_baseUrl" name="baseUrl"
                                              type="text"/></div>
<!--                     <div class='input'><input placeholder="requid" name="requid" type="uid" style="width:80px;" autocomplete="off" /></div> -->
                    <div class='input'><input placeholder="api_token" id="input_apiKey" name="apiKey" type="text" /></div>
                    <div class='input'><a id="explore" href="#">登陆</a></div>
                </form>
            </div>
        </div>
        <div class="swagger-ui-wrap" style="height:30px;line-height:30px;padding:10px;font-size:12px;">
            <div style="float:right;padding:0 10px 0 0;border:1px dashed #ccc;text-align: center;">
                <span style="color:#ccc;margin-right:10px;padding:0 10px;background:#f8f8f8;display:inline-block;">接口状态</span>
                <span class="api-status-init" >拟定（未完成/返回假数据）</span> <span style="color:#ccc;">¦</span>&nbsp;
                <span class="api-status-finished">定稿(已完成)</span> <span style="color:#ccc;">¦</span>&nbsp;
                <span class="api-status-modified">更新(重要修改)</span>
            </div>
            <div class="clr"></div>
        </div>

        <div id="swagger-ui-container" class="swagger-ui-wrap">

        </div>
        <div id="message-bar" class="swagger-ui-wrap"></div>
    </body>
</html>
# 软件要求

本系统采用 LNMP 环境进行部署。软件版本如下：

1. Linux
2. Nginx >= 1.12.2
3. MySQL >= 5.6.38
4. PHP >= 7.1.11
5. Redis >= 3.2.11
6. Nodejs >= 8.9.2

需要的 PHP 扩展：

php-cli, php-common, php-fpm, php-gd, php-mbstring, php-mcrypt, php-mysqlnd, php-opcache, php-pdo, php-xml

其他相关软件：

composer, pm2

# 搭建环境

## 部署 HTTP Server

1. composer install
2. 创建 .env 文件：`php -r "file_exists('.env') || copy('.env.example', '.env');"`
3. 生成 APP_KEY：`php artisan key:generate`
4. [配置 .env](https://gitee.com/Yun_WuYu/wawaji_backend/wikis/%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E)
5. 创建数据表：`php artisan migrate`
6. 创建 public/storage 到 storage/app/public 的符号链接：php artisan storage:link
7. 修改文件权限：
    1. chown -R apache:apache storage
    2. chown -R apache:apache bootstrap/cache
    3. chmod +x app/Services/QCloud/tea_decode
    4. chmod +x app/Services/QCloud/tea_encode
8. 缓存配置信息（可选）：php artisan config:cache
9. 缓存路由信息（可选）：php artisan route:cache
10. 配置腾讯云公私钥文件，路径：storage/app/services/qcloud
11. 配置微信公众号支付证书，路径：storage/app/services/wechat/official_account
12. 配置微信移动应用支付证书路径：storage/app/services/wechat/mobile_app
13. 部署“消息队列”的守护进程：`nohup php /path/to/project/artisan queue:work --tries=3 > /dev/null 2>&1 &`
14. 部署“检查玩家排队”的守护进程：`nohup php /path/to/project/artisan mall:check-player-queues > /dev/null 2>&1 &`
15. 部署计划任务：`* * * * * php /path/to/project/artisan schedule:run >> /dev/null 2>&1`

注意：
1. 消息队列可以根据任务量，增加进程数
2. 检查玩家排队只能启动一个进程
3. 守护进程需要进行监控，进程发生退出时，可重启。参考：https://d.laravel-china.org/docs/5.5/queues#supervisor-configuration
4. `缓存配置信息` 和 `缓存路由信息`，在开发、调试环境下不需要启用。这两项启用后，如果 配置、路由 有相关改动，需要重新缓存

## 部署 Socket Server

1. 安装 laravel-echo-server ：`npm install -g laravel-echo-server`
2. 创建一个目录，在其中新建两个文件：
    1. [laravel-echo-server.json](https://gitee.com/Yun_WuYu/wawaji_backend/wikis/pages?title=%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E&parent=#laravel-echo-serverjson)
    2. [pm2.json](https://gitee.com/Yun_WuYu/wawaji_backend/wikis/pages?title=%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E&parent=#pm2json)
3. 运行 Socket Server ：`pm2 start pm2.json`

## 业务初始化

1. 导入省市区数据：`php artisan mall:import-regions`
2. 创建管理员：`php artisan mall:create-admin {username} {password}` ，例如：`php artisan mall:create-admin admin 123456`
3. 创建充值项：`php artisan mall:create-coins`

## 在“硬件系统”上配置回调地址

1. 硬件系统游戏结果回调： `http://api.domain.com/api/callback/game-records`
2. 硬件系统设备状态回调： `http://api.domain.com/api/callback/machines`
3. 硬件系统订单回调： `http://api.domain.com/api/callback/orders`

注：将 `http://api.domain.com` 改成自己的域名

## nginx 配置

```nginx
server {
    listen   80;
    root   /path/to/project/public;
    server_name api.domain.com;
    index  index.php index.html;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        add_header 'Access-Control-Allow-Origin' $http_origin always;
        add_header 'Access-Control-Allow-Credentials' true always;
        add_header 'Access-Control-Allow-Headers' 'X-CSRF-TOKEN, X-XSRF-TOKEN, Authorization, Origin, X-Requested-With, Content-Type, Accept, X-App-Version, X-App-Client, X-Socket-ID' always;

        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' $http_origin always;
            add_header 'Access-Control-Allow-Credentials' true always;
            add_header 'Access-Control-Allow-Headers' 'X-CSRF-TOKEN, X-XSRF-TOKEN, Authorization, Origin, X-Requested-With, Content-Type, Accept, X-App-Version, X-App-Client, X-Socket-ID' always;
            add_header Access-Control-Max-Age 2592000;

            access_log off;
            expires 30d;

            return 204;
        }

        include        fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_pass   127.0.0.1:9000;
    }

    location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$ {
        expires    1d;
    }

    location ~ .*\.(js|css|html|htm)?$ {
        expires    12h;
    }
}
```

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
     */

    'doll' => [
        'env' => env('DOLL_ENV'), // 娃娃机系统环境
        'video_scheme' => env('DOLL_VIDEO_SCHEME'),   // 视频方案：qiyiguo, web-camera, qcloud, zego
        'platform' => env('DOLL_PLATFORM'), // 娃娃机合作方
        'secret' => env('DOLL_SECRET'), // 娃娃机密钥
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'qcloud' => [
        'app_id' => env('QCLOUD_APP_ID'), // 腾讯云 sdk app id
        'secret' => env('QCLOUD_SECRET'),
        'account_type' => env('QCLOUD_ACCOUNT_TYPE'), // 账户类型
        'bizid' => env('QCLOUD_BIZID'),
        'keys_path' => env('QCLOUD_KEYS_PATH', storage_path('app/services/qcloud')), // 公私钥路径
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'zego' => [
        'app_id' => env('ZEGO_APP_ID'), // 即构 app id
        'server' => env('ZEGO_SERVER'),
    ]

];

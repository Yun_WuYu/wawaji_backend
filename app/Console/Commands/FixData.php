<?php

namespace App\Console\Commands;

use App\Models\Machine;
use App\Models\CoinRecord;
use App\Models\Payment;
use App\Models\User;
use App\Models\Coin;
use App\Models\Order;
use App\Services\Doll;
use Illuminate\Console\Command;

class FixData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mall:fix-data {method : 执行修复的方法}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '用于开发迭代中，修复应用数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $method = $this->argument('method');

        $this->$method();
    }
    
    /**
     * 修复用户支付数据
     */
    protected function userPayment()
    {
        $users = User::get();

        foreach ($users as $user) {
            $userId = $user->id;

            $costMoney = Payment::where(['user_id' => $userId, 'status' => Payment::STATUS_PAID])
                ->sum('money');
            $coinBuy = CoinRecord::where(['user_id' => $userId, 'recordable_type' => CoinRecord::TYPE_PAYMENT])
                ->sum('coin');

            // 更新用户的总的购买游戏币、总消费额
            User::where('id', $userId)->update([
                'coin_buy' => $coinBuy,
                'cost_money' => $costMoney,
            ]);
        }

        $this->info('已完成 - 修复用户支付数据');
    }

    /**
     * 修复游戏机设备号
     */
    protected function machineDeviceSn()
    {
        $doll = app(Doll::class);
        $machines = Machine::get();

        foreach ($machines as $machine) {
            $result = $doll->getDeviceStatus($machine->device_id);

            if ($result) {
                $deviceSn = $result['device_sn'];

                // 从硬件系统同步游戏机的设备编号
                $machine->update([
                    'device_sn' => $deviceSn,
                ]);
            }
        }

        $this->info('已完成 - 修复游戏机设备号');
    }

    /**
     * 整理用户支付流水记录的游戏币到payment表
     */
    protected function userPaymentCoin()
    {
        $payments = Payment::where([
            'status' => Payment::STATUS_PAID,
            'coin' => 0,
        ])->get();

        foreach ($payments as $payment) {
            $paymentId = $payment->id;

            $coin = CoinRecord::where([
                'recordable_id' => $paymentId,
                'recordable_type' => CoinRecord::TYPE_PAYMENT,
            ])->value('coin');

            // 更新用户支付流水的游戏币到payment表
            Payment::where('id', $paymentId)->update([
                'coin' => $coin,
            ]);
        }

        $this->info('已完成 - 修复支付记录数据');
    }

    /**
     * 修复充值项数据，新增 8 条记录，共 10 条
     */
    public function coins()
    {
        $coins = [
            ['coin' => 100, 'money' => 10, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 200, 'money' => 15, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 300, 'money' => 20, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 400, 'money' => 25, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 500, 'money' => 30, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 600, 'money' => 35, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 700, 'money' => 40, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 800, 'money' => 45, 'status' => Coin::STATUS_DISABLE],
        ];

        foreach ($coins as $item) {
            Coin::create($item);
        }

        $this->info('已完成 - 成功修复充值项');
    }

    /**
     * 修复订单的发货方式为自行发货
     */
    public function orders()
    {
        $orders = Order::withTrashed()->where('send_type', 0)->get();

        foreach ($orders as $order) {
            $order->update(['send_type' => Order::SEND_TYPE_SELF]);
        }

        $this->info('已完成 - 修复订单记录数据');
    }
}

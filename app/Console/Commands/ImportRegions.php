<?php

namespace App\Console\Commands;

use App\Models\Region;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportRegions extends Command
{
    /**
     * 命令名称及参数
     *
     * @var string
     */
    protected $signature = 'mall:import-regions';

    /**
     * 命令描述
     *
     * @var string
     */
    protected $description = '导入省市区数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 执行命令
     *
     * @return mixed
     */
    public function handle()
    {
        if (Region::exists()) {
            $this->info('regions 表已存在数据');
            return;
        }
        
        $sql = file_get_contents(database_path('seeds/regions.sql'));
        
        DB::insert($sql);
        
        $this->info('已完成 - regions 导入');
    }
}

<?php

namespace App\Console\Commands;

use App\Common\Machine\PlayerQueue;
use App\Events\MachineStatusChanged;
use App\Models\GameRecord;
use App\Models\Machine;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * 对单次游戏时间超过 1 分钟的游戏机状态进行复位
 *
 * 游戏机硬件系统有时会没有结果回调，导致游戏机无法复位。
 * 此处轮询检查，执行复位操作。
 *
 * Class ResetDeviceStatus
 * @package App\Console\Commands
 */
class ResetDeviceStatus extends Command
{
    /**
     * 命令名称及参数
     *
     * @var string
     */
    protected $signature = 'mall:reset-device-status';

    /**
     * 命令描述
     *
     * @var string
     */
    protected $description = '重置游戏机状态';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * 执行命令
     */
    public function handle()
    {
        // 将游戏时间超过1分钟的游戏机的状态复位
        $machines = Machine::where([
            ['device_status', '=', Machine::DEVICE_STATUS_WORKING],
            ['played_at', '<', Carbon::now()->subMinute()],
        ])->get();
        
        foreach ($machines as $machine) {
            $machine->update(['device_status' => Machine::DEVICE_STATUS_IDLE]);
            
            // 清除“游戏机开始权限”的占用标记
            $playerQueue = new PlayerQueue($machine);
            $playerQueue->clearOccupancyMark();
            
            $record = GameRecord::find($machine->game_record_id);
            
            // 广播事件：游戏机状态改变
            event(new MachineStatusChanged($machine, $record));
        }
    }
}

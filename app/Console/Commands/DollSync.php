<?php

namespace App\Console\Commands;

use App\Models\Camera;
use App\Models\Machine;
use App\Services\Doll;
use App\Services\QCloud\QCloud;
use Illuminate\Console\Command;

/**
 * 从硬件系统同步娃娃机信息
 *
 * Class DollSync
 * @package App\Console\Commands
 */
class DollSync extends Command
{
    /**
     * 命令名称及参数
     *
     * @var string
     */
    protected $signature = 'mall:doll-sync';

    /**
     * 命令描述
     *
     * @var string
     */
    protected $description = '从硬件系统同步娃娃机信息，目前只同步新机器';
    
    /**
     * 硬件系统服务
     *
     * @var Doll
     */
    protected $doll;
    
    /**
     * 腾讯云服务
     *
     * @var QCloud
     */
    protected $qcloud;

    /**
     * Create a new command instance.
     *
     * @param Doll $doll
     * @param QCloud $qcloud
     * @return void
     */
    public function __construct(Doll $doll, QCloud $qcloud)
    {
        parent::__construct();
        
        $this->doll = $doll;
        $this->qcloud = $qcloud;
    }
    
    /**
     * 执行命令
     *
     * @throws \Exception
     */
    public function handle()
    {
        // 分页获取机器信息，并完成同步
        $page = 1;
        $perPage = 100;
        
        do {
            $result = $this->doll->dollList($page, $perPage);
            
            $dolls = $result['list'];
            $this->syncDevices($dolls);
            
            $count = count($dolls);
            $page++;
        } while ($count >= $perPage);
        
        $this->info('已完成 - 同步设备信息');
    }
    
    /**
     * 同步设备信息
     *
     * @param array $dolls
     */
    public function syncDevices($dolls)
    {
        $videoScheme = doll_video_scheme();
        
        $devices = array_column($dolls, null, 'device_id');
        $deviceIds = array_keys($devices);
        
        $existDeviceIds = Machine::whereIn('device_id', $deviceIds)->pluck('device_id')->toArray();
        
        foreach ($devices as $deviceId => $device) {
            if (in_array($deviceId, $existDeviceIds)) {
                continue;
            }
            
            switch ($videoScheme) {
                case 'qiyiguo' :
                    $this->createQiyiguoMachine($device);
                    break;
                case 'qcloud' :
                    $this->createQCloudMachine($device);
                    break;
                case 'web-camera' :
                    $this->createWebCameraMachine($device);
                    break;
                case 'zego' :
                    $this->createZegoMachine($device);
                    break;
                default :
                    $this->error('视频方案配置不正确');
                    break;
            }
        }
    }

    /**
     * 创建“奇艺果视频方案”的娃娃机
     *
     * @param array $device
     */
    public function createQiyiguoMachine($device)
    {
        // 创建娃娃机
        Machine::create([
            'name' => $device['name'],
            'ware_name' => $device['goods_name'],
            'device_sn' => $device['device_sn'],
            'device_id' => $device['device_id'],
            'device_status' => $device['status'],
            'room_id' => $device['room_id'],
            'ware_image' => $device['img'],
        ]);
    }
    
    /**
     * 创建“腾讯云视频方案”的娃娃机
     *
     * @param array $device
     */
    public function createQCloudMachine($device)
    {
        // 创建娃娃机
        $machine = Machine::create([
            'name' => $device['name'],
            'ware_name' => $device['goods_name'],
            'device_sn' => $device['device_sn'],
            'device_id' => $device['device_id'],
            'room_id' => $device['room_id'],
            'device_status' => $device['status'],
            'ware_image' => $device['img'],
        ]);
        
        $streamFront = $this->qcloud->getStreamAddress($machine->room_id, $device['camera_front']);
        $streamSide = $this->qcloud->getStreamAddress($machine->room_id, $device['camera_side']);
        
        // 每台机器分配两个摄像头：正面 和 侧面
        $machine->cameras()->createMany([
            [
                'type' => Camera::TYPE_FRONT,
                'live_openid' => $device['camera_front'],
                'rtmp_address' => $streamFront['rtmp'],
                'hls_address' => $streamFront['hls'],
            ],
            [
                'type' => Camera::TYPE_SIDE,
                'live_openid' => $device['camera_side'],
                'rtmp_address' => $streamSide['rtmp'],
                'hls_address' => $streamSide['hls'],
            ],
        ]);
    }
    
    /**
     * 创建“网络摄像头方案”的娃娃机
     *
     * @param array $device
     */
    public function createWebCameraMachine($device)
    {
        // 创建娃娃机
        $machine = Machine::create([
            'name' => $device['name'],
            'ware_name' => $device['goods_name'],
            'device_sn' => $device['device_sn'],
            'device_id' => $device['device_id'],
            'device_status' => $device['status'],
            'ware_image' => $device['img'],
        ]);
        
        // 每台机器分配两个摄像头：正面 和 侧面
        $machine->cameras()->createMany([
            [
                'type' => Camera::TYPE_FRONT,
                'rtmp_address_main' => $device['stream_address_1'] ?? '',  // 操作者的rtmp流地址
                'rtmp_address' => $device['stream_address_raw_1'] ?? '',   // 围观的rtmp流地址
            ],
            [
                'type' => Camera::TYPE_SIDE,
                'rtmp_address_main' => $device['stream_address_2'] ?? '',  // 操作者的rtmp流地址
                'rtmp_address' => $device['stream_address_raw_2'] ?? '',   // 围观的rtmp流地址
            ],
        ]);
    }
    
    /**
     * 创建“即构视频方案”的娃娃机
     *
     * @param array $device
     */
    public function createZegoMachine($device)
    {
        // 创建娃娃机
        $machine = Machine::create([
            'name' => $device['name'],
            'ware_name' => $device['goods_name'],
            'device_sn' => $device['device_sn'],
            'device_id' => $device['device_id'],
            'device_status' => $device['status'],
            'room_id' => $device['room_id'],
            'ware_image' => $device['img'],
        ]);
        
        // 每台机器分配两个摄像头：正面 和 侧面
        $machine->cameras()->createMany([
            [
                'type' => Camera::TYPE_FRONT,
                'live_openid' => $device['camera_front'],
            ],
            [
                'type' => Camera::TYPE_SIDE,
                'live_openid' => $device['camera_side'],
            ],
        ]);
    }
}

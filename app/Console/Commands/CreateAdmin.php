<?php

namespace App\Console\Commands;

use App\Models\Admin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mall:create-admin {username : 用户名} {password : 密码}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建管理员';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inputs = array_only($this->arguments(), ['username', 'password']);

        Validator::make($inputs, [
            'username' => 'required|max:50',
            'password' => 'required|max:20'
        ])->validate();

        $inputs['password'] = bcrypt($inputs['password']);

        $user = new Admin($inputs);
        $user->refreshApiToken();
        $user->save();

        $this->info("操作成功，用户 id 为 {$user->id}");
    }
}

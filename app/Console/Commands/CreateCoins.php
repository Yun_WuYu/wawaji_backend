<?php

namespace App\Console\Commands;

use App\Models\Coin;
use Illuminate\Console\Command;

class CreateCoins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mall:create-coins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建充值项';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coins = [
            ['coin' => 100, 'money' => 10, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 200, 'money' => 15, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 300, 'money' => 20, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 400, 'money' => 25, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 450, 'money' => 30, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 500, 'money' => 35, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 550, 'money' => 40, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 600, 'money' => 45, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 650, 'money' => 50, 'status' => Coin::STATUS_DISABLE],
            ['coin' => 700, 'money' => 55, 'status' => Coin::STATUS_DISABLE],
        ];

        foreach ($coins as $item) {
            Coin::create($item);
        }

        $this->info('已完成 - 成功创建充值项');
    }
}

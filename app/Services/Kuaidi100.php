<?php

namespace App\Services;

use GuzzleHttp\Client;

/**
 * 快递100物流信息
 *
 * Class Kuaidi100
 * @package App\Services
 */
class Kuaidi100
{
    private $url = 'http://www.kuaidi100.com/query';

    /**
     * 物流查询
     *
     * @param  string $shippingCom
     * @param  string $shippingNo
     * @return array
     */
    public function traceInfo($shippingCom, $shippingNo)
    {
        $client = new Client(['timeout' => 10]);

        $params = [
            'id' => 1,
            'type' => $shippingCom,
            'postid' => $shippingNo,
        ];

        $result = $client->get($this->url, ['query' => $params]);

        $contents = json_decode($result->getBody()->getContents(), true);

        return $contents;
    }
}

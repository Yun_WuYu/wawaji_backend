<?php

namespace App\Services;

use EasyWeChat\Foundation\Application;

class WeChat
{
    protected $config;

    public function __construct()
    {
        $this->config = config('wechat');
    }

    /**
     * 公众号
     *
     * @return \EasyWeChat\Foundation\Application
     */
    public function officialAccount()
    {
        return new Application($this->config);
    }

    /**
     * 小程序
     *
     * @return \EasyWeChat\Foundation\Application
     */
    public function miniProgram()
    {
        $config = array_merge($this->config, $this->config['mini_program']);

        return new Application($config);
    }

    /**
     * 移动应用
     *
     * @return \EasyWeChat\Foundation\Application
     */
    public function mobileApp()
    {
        $config = array_merge($this->config, $this->config['mobile_app']);
        $config['payment'] = $this->config['mobile_app_payment'];

        return new Application($config);
    }
}

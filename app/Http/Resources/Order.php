<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Order extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Order $order */
        $order = $this->resource;

        $data = $order->attributesToArray();
        $extension = $order->extension->attributesToArray();

        $data['wares'] = $this->wares;
        $data['extension'] = array_merge($extension, $order->extension->regions());

        return $data;
    }
}

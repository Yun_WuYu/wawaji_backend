<?php

namespace App\Http\Resources;

use App\Support\ResourceCollection;

class CoinRecords extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($record) {
            /** @var \App\Models\CoinRecord $record */
            
            return [
                'coin' => $record->coin,
                'recordable_type' => $record->recordable_type,
                'created_at' => (string) $record->created_at,
            ];
        })->all();
    }
}

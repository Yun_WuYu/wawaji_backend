<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Account extends Resource
{
    protected $withApiToken;
    
    public function __construct($resource, $withApiToken = false)
    {
        parent::__construct($resource);
        
        $this->withApiToken = $withApiToken;
    }
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\User $user */
        $user = $this->resource;
        
        $data = [
            'id' => $user->id,
            'nickname' => $user->nickname,
            'coin' => $user->coin,
            'coin_buy' => $user->coin_buy,
            'avatar' => image_url($user->avatar),
            'live_openid' => $user->liveOpenId(),
        ];
        
        $scheme = doll_video_scheme();
        $data['video_token'] = $user->getVideoToken($scheme);

        // 兼容 v2.2.1 之前的旧版本
        if ('qcloud' == $scheme) {
            $data['qcloud_sig'] = $data['video_token'];
        }
        
        if ($this->withApiToken) {
            $data['api_token'] = $user->api_token;
        }
        
        return $data;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Address extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Address $address */
        $address = $this->resource;
        
        $province = $address->province;
        $city = $address->city;
        $district = $address->district;
        
        return [
            'id' => $address->id,
            'address' => $address->address,
            'consignee' => $address->consignee,
            'phone' => $address->phone,
            'is_default' => $address->is_default,
            'province' => [
                'id' => $province->id,
                'name' => $province->name,
            ],
            'city' => [
                'id' => $city->id,
                'name' => $city->name,
            ],
            'district' => [
                'id' => $district->id,
                'name' => $district->name,
            ],
        ];
    }
}

<?php

namespace App\Http\Resources;

use App\Support\ResourceCollection;

class DollRecords extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return $this->collection->map([$this, 'item'])->all();
    }
    
    /**
     * 格式化单个数据
     *
     * @param \App\Models\GameRecord $record
     * @return array
     */
    public function item($record)
    {
        $item = object_only($record, [
            'id',
            'ware_id',
            'ware_name',
            'ware_image',
            'ware_status',
        ]);
        
        $item['created_at'] = (string) $record->created_at;
        
        return $item;
    }
}

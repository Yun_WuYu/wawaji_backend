<?php

namespace App\Http\Resources\Admin;

use App\Support\ResourceCollection;

class Payments extends ResourceCollection
{
    private static $fetchKeys = [
        'id', 'user_id', 'trade_no', 'coin', 'money'
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map([$this, 'item'])->all();
    }

    /**
     * 格式化单个数据
     *
     * @param \App\Models\Payment $payment
     * @return array
     */
    public function item($payment)
    {
        $item = object_only($payment, self::$fetchKeys);

        $item['coin'] = $payment->coin;
        $item['nickname'] = $payment->user->nickname;
        $item['created_at'] = (string) $payment->created_at;

        return $item;
    }
}

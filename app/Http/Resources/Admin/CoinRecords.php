<?php

namespace App\Http\Resources\Admin;

use App\Support\ResourceCollection;

class CoinRecords extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map([$this, 'item'])->all();
    }
    
    /**
     * 单个数据的转换
     *
     * @param \App\Models\CoinRecord $record
     * @return array
     */
    public function item($record)
    {
        $user = $record->user;
        
        return [
            'coin' => $record->coin,
            'recordable_type' => $record->recordable_type,
            'created_at' => (string) $record->created_at,
            'user' => [
                'id' => $user->id,
                'nickname' => $user->nickname,
            ]
        ];
    }
}

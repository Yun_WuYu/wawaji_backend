<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\Resource;

class Machine extends Resource
{
    private static $fetchKeys = [
        'device_sn', 'name', 'ware_name', 'coin'
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\machine $machine */
        $machine = $this->resource;

        $item = object_only($machine, self::$fetchKeys);

        $item['detail_image'] = image_url($machine->detail_image);
        $item['ware_image'] = $machine->ware_image;

        return $item;
    }
}

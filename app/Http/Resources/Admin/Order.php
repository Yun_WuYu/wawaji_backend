<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\Resource;

class Order extends Resource
{
    private static $fetchKeys = [
        'id', 'order_sn', 'out_order_id', 'send_type', 'status'
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\order $order */
        $order = $this->resource;

        $item = object_only($order, self::$fetchKeys);
        $extension = $order->extension->attributesToArray();

        $item['created_at'] = (string) $order->created_at;
        $item['wares'] = $order->wares;
        $item['extension'] = array_merge($extension, $order->extension->regions());

        return $item;
    }
}

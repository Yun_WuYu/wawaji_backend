<?php

namespace App\Http\Resources\Admin;

use App\Support\ResourceCollection;

class Machines extends ResourceCollection
{
    private static $fetchKeys = [
        'id', 'name', 'device_sn', 'ware_name',  'coin', 'device_status',  'is_display', 'stick_time'
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map([$this, 'item'])->all();
    }

    /**
     * 格式化单个数据
     *
     * @param \App\Models\Machine $machine
     * @return array
     */
    public function item($machine)
    {
        $item = object_only($machine, self::$fetchKeys);

        $item['detail_image'] = image_url($machine->detail_image);

        return $item;
    }
}

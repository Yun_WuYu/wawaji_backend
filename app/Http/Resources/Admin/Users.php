<?php

namespace App\Http\Resources\Admin;

use App\Support\ResourceCollection;

class Users extends ResourceCollection
{
    private static $fetchKeys = [
        'id', 'nickname', 'cost_money', 'coin_buy', 'coin'
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map([$this, 'item'])->all();
    }

    /**
     * 格式化单个数据
     *
     * @param \App\Models\User $user
     * @return array
     */
    public function item($user)
    {
        $item = object_only($user, self::$fetchKeys);

        $item['created_at'] = (string) $user->created_at;

        return $item;
    }
}

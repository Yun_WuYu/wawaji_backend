<?php

namespace App\Http\Resources\Admin;

use App\Support\ResourceCollection;

class GameRecords extends ResourceCollection
{
    private static $fetchKeys = [
       'id', 'user_id', 'coin', 'operate_result'
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map([$this, 'item'])->all();
    }

    /**
     * 格式化单个数据
     *
     * @param \App\Models\gameRecord $gameRecord
     * @return array
     */
    public function item($gameRecord)
    {
        $item = object_only($gameRecord, self::$fetchKeys);

        $item['device_sn'] = $gameRecord->machine->device_sn;
        $item['name'] = $gameRecord->machine->name;
        $item['nickname'] = $gameRecord->user->nickname;
        $item['updated_at'] = (string) $gameRecord->updated_at;

        return $item;
    }
}

<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\Resource;

class Banner extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Banner $banner */
        $banner = $this->resource;
        
        return [
            'id' => $banner->id,
            'is_display' => $banner->is_display,
            'image' => image_url($banner->image),
        ];
    }
}

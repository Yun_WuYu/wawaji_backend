<?php

namespace App\Http\Resources\Admin;

use App\Support\ResourceCollection;

class Orders extends ResourceCollection
{
    private static $fetchKeys = [
        'id', 'order_sn', 'out_order_id', 'send_type', 'status'
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return $this->collection->map([$this, 'item'])->all();
    }

    /**
     * 格式化单个数据
     *
     * @param \App\Models\Order $order
     * @return array
     */
    public function item($order)
    {
        $item = object_only($order, self::$fetchKeys);

        $extension = $order->extension->attributesToArray();

        $item['created_at'] = (string) $order->created_at;
        $item['wares'] =  $order->wares;
        $item['extension'] = array_merge($extension, $order->extension->regions());

        return $item;
    }
}

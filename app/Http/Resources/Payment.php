<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Payment extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\Payment $payment */
        $payment = $this->resource;
        
        return object_only($payment, ['id', 'trade_no', 'money', 'status']);
    }
}

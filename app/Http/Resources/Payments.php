<?php

namespace App\Http\Resources;

use App\Support\ResourceCollection;

class Payments extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map([$this, 'item'])->all();
    }

    /**
     * 格式化单个数据
     *
     * @param \App\Models\Payment $payment
     * @return array
     */
    public function item($payment)
    {
        $item = object_only($payment, [
            'id',
            'user_id',
            'coin',
            'money',
        ]);

        $item['created_at'] = (string) $payment->created_at;

        return $item;
    }
}

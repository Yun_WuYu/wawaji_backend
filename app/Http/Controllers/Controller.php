<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * 包裹数据
     *
     * @param null|mixed $data 为 null 时，会包裹一个空对象
     * @param string $wrap
     * @return array
     */
    public function wrapData($data = null, $wrap = 'data')
    {
        if (is_null($data)) {
            $data = new \stdClass();
        }
        
        return [$wrap => $data];
    }
}

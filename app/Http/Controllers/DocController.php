<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocController extends Controller
{
    private $username;
    private $password;

    public function __construct()
    {
        $this->username = env('API_DOC_USER', 'test');
        $this->password = env('API_DOC_PASSWORD', uniqid());
    }

    /**
     * 接口文档
     *
     * @param Request $request
     * @param int|string $admin 是否为 admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $admin = 0)
    {
        // basic auth
        if ($request->getUser() != $this->username || $request->getPassword() != $this->password) {
            return response('Unauthorized', 401, [
                'WWW-Authenticate' => 'Basic realm="Artqiyi inc."'
            ]);
        }

        $data = $admin ? [
            'title' => 'H5 管理后台',
            'resourcePath' => 'rs_admin/resources.json',
        ] : [
            'title' => 'H5 客户端',
            'resourcePath' => 'rs/resources.json',
        ];

        return view('apidoc', $data);
    }
}

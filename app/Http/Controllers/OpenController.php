<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\GameRecord;

class OpenController extends Controller
{
    /**
     * 配置接口
     *
     * @return array
     */
    public function config()
    {
        $config = [
            'app_name' => get_option('app_name'),
            'auditing_version' => $this->auditingVersion(),
            'mall_url' => config('mall.url'),
            'fake_pay' => config('mall.fake_pay'),
            'socket_server_host' => config('mall.socket_server_host'),
        ];
        
        $configDoll = config('services.doll');
        $config['doll_env'] = $configDoll['env'];
        $config['video_scheme'] = $configDoll['video_scheme'];

        if ('qcloud' == $config['video_scheme']) {
            $config['qcloud'] = array_only(config('services.qcloud'), [
                'app_id', 'account_type'
            ]);
        } elseif ('zego' == $config['video_scheme']) {
            $config['zego'] = config('services.zego');
        }

        if ('web' == app_client()) {
            // 分享数据
            $share = [
                'share_title' => get_option('share_title'),
                'share_image' => image_url(get_option('share_image')),
                'share_description' => get_option('share_description'),
            ];
            $config += $share;
        }
        
        return $this->wrapData($config);
    }
    
    /**
     * 根据客户端返回审核中的版本号
     *
     * @return string
     */
    protected function auditingVersion()
    {
        $client = app_client();
        
        if ('android' == $client) {
            return get_option('auditing_version_android');
        } elseif ('ios' == $client) {
            return get_option('auditing_version_ios');
        } else {
            return '';
        }
    }
    
    /**
     * 轮播图
     *
     * @return array
     */
    public function banners()
    {
        $banners = Banner::where('is_display', 1)->get();

        $data = [];
        foreach($banners as $banner) {
            $data[] = image_url($banner->image);
        }
        
        return $this->wrapData($data);
    }

    /**
     * 首页最新抓中记录展示
     *
     * @return array
     */
    public function dollRecords()
    {
        $gameRecords = GameRecord::with('user')->where('operate_result', GameRecord::OPERATE_SUCCESS)
            ->orderByDesc('id')->limit(10)->get();

        $sentence = get_option('doll_records_sentences');
        $sentences = explode("\n", $sentence);

        $data = $gameRecords->map(function ($gameRecord) use ($sentences) {
            return [
                'id' => $gameRecord->id,
                'ware_name' => $gameRecord->ware_name,
                'ware_image' => $gameRecord->ware_image,
                'sentence' => $sentences[array_rand($sentences)],
                'user' => [
                    'nickname' => $gameRecord->user->nickname,
                    'avatar' => image_url($gameRecord->user->avatar),
                ],
            ];
        });

        return $this->wrapData($data);
    }
}

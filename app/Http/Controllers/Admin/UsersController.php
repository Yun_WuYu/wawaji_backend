<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Users;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * 用户列表
     *
     * @param Request $request
     * @return Users
     */
    public function index(Request $request)
    {
        $perPage = per_page();

        $filter = [];

        if ($request->filled('nickname')) {
            $filter['nickname'] = $request->get('nickname');
        }

        $paginate = User::where($filter)->orderByDesc('id')->paginate($perPage);

        return new Users($paginate);
    }
}

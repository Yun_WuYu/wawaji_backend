<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\GameRecords;
use App\Models\GameRecord;
use App\Models\Machine;
use Illuminate\Http\Request;

class GameRecordsController extends Controller
{
    /**
     * 游戏记录列表
     *
     * @param Request $request
     * @return GameRecords
     */
    public function index(Request $request)
    {
        $perPage = per_page();

        $filter = [];

        if ($request->filled('operate_result')) {
            $filter['operate_result'] = $request->get('operate_result');
        }

        if ($request->filled('device_sn')) {
            $deviceSn = $request->get('device_sn');
            $machineId = Machine::where('device_sn', $deviceSn)->value('id');
            $filter['machine_id'] = $machineId;
        }

        $paginate = GameRecord::with(['user', 'machine'])->where($filter)->orderByDesc('id')->paginate($perPage);

        return new GameRecords($paginate);
    }
}

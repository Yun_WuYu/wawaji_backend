<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Order as OrderResource;
use App\Http\Resources\Admin\Orders;
use App\Models\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * 订单列表
     *
     * @param Request $request
     * @return Orders
     */
    public function index(Request $request)
    {
        $filter = [];

        if ($request->filled('status')) {
            $filter['status'] = $request->get('status');
        }
        if ($request->filled('order_sn')) {
            $filter['order_sn'] = $request->get('order_sn');
        }
        if ($request->filled('send_type')) {
            $filter['send_type'] = $request->get('send_type');
        }

        $paginate = Order::where($filter)->orderByDesc('id')
            ->with('wares', 'extension')->withTrashed()->paginate(per_page());

        return new Orders($paginate);
    }

    /**
     * 订单详情
     *
     * @param int $id
     * @return OrderResource
     */
    public function show($id)
    {
        $order = Order::withTrashed()->findOrFail($id);
        return new OrderResource($order);
    }
}

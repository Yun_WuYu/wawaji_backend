<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Payments;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * 充值列表
     *
     * @param Request $request
     * @return \App\Http\Resources\Admin\Payments
     */
    public function index(Request $request)
    {
        $perPage = per_page();
        
        $filter = ['status' => Payment::STATUS_PAID];
        if ($request->filled('trade_no')) {
            $filter['trade_no'] = $request->get('trade_no');
        }

        $paginate = Payment::where($filter)->with('user')
            ->orderByDesc('id')->paginate($perPage);

        return new Payments($paginate);
    }

    /**
     * 支付统计
     *
     * @return array
     */
    public function recharge()
    {
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();

        // 今日充值总金额
        $today = Payment::whereBetween('paid_at', [$today, $tomorrow])
            ->where('status', Payment::STATUS_PAID)->sum('money');

        // 充值总金额
        $total = Payment::where('status', Payment::STATUS_PAID)->sum('money');

        $data = [
            'today' => $today,
            'total' => $total,
        ];

        return $this->wrapData($data);
    }
}

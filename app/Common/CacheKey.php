<?php

namespace App\Common;

/**
 * 统一管理 cache key
 *
 * Class CacheKey
 * @package App\Common
 */
class CacheKey
{
    public static $options = 'options';
    
    public static $regions = 'regions';
    
    /**
     * 微信授权码
     *
     * @param string $code
     * @return string
     */
    public static function wechatOauthCode($code)
    {
        return "wechat:oauth_code:{$code}";
    }

    /**
     * 用户的视频 token
     *
     * @param int $userId
     * @return string
     */
    public static function userVideoToken($userId)
    {
        return "users:{$userId}:video_token";
    }
    
    /**
     * 房间的腾讯云 sig
     *
     * @param int $machineId
     * @return string
     */
    public static function machineQCloudSig($machineId)
    {
        return "machines:{$machineId}:qcloud_sig";
    }
    
    /**
     * 游戏机玩家队列第一个用户的id
     *
     * @param int $machineId
     * @return string
     */
    public static function machinePlayerQueueFirstUserId($machineId)
    {
        return "machines:{$machineId}:player_queue:first_user_id";
    }
    
    /**
     * 游戏机玩家队列第一个用户的记录时间，即记录`machinePlayerQueueFirstUserId`值的时间
     *
     * @param int $machineId
     * @return string
     */
    public static function machinePlayerQueueFirstUserTime($machineId)
    {
        return "machines:{$machineId}:player_queue:first_user_time";
    }
}

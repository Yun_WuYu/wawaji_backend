<?php

namespace App\Models;

use App\Common\CacheKey;
use App\Services\Doll;
use App\Support\Time;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property string $api_token
 * @property string $avatar 头像
 * @property string $nickname 昵称
 * @property string $gender 性别：0为未知，1为男，2为女
 * @property int $coin 游戏币
 * @property int $coin_buy 总的购买的游戏币个数
 * @property float $cost_money 消费总金额
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'api_token',
    ];

    /**
     * 地址列表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * 订单列表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * 游戏币记录
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coinRecords()
    {
        return $this->hasMany(CoinRecord::class);
    }

    /**
     * 游戏记录
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gameRecords()
    {
        return $this->hasMany(GameRecord::class);
    }

    /**
     * 默认地址
     *
     * @return \App\Models\Address|null|static
     */
    public function defaultAddress()
    {
        return $this->addresses()->where('is_default', 1)->first();
    }

    /**
     * 直播 openid
     *
     * @return int
     */
    public function liveOpenId()
    {
        return $this->id;
    }

    /**
     * 获取视频 token
     *
     * @param string $videoScheme
     * @return string
     */
    public function getVideoToken($videoScheme)
    {
        $key = CacheKey::userVideoToken($this->id);

        // 缓存3个月
        return Cache::remember($key, 3 * Time::MINUTES_OF_MONTH, function () use ($videoScheme) {
            $doll = app(Doll::class);
            $openId = $this->liveOpenId();
            $token = null;

            switch ($videoScheme) {
                case 'qiyiguo' :
                    $token = $doll->getQiyiguoToken($openId);
                    break;
                case 'qcloud' :
                    $token = $doll->getQcloudToken($openId);
                    break;
                case 'zego' :
                    $token = $doll->getZegoToken($openId);
                    break;
                default :
                    break;
            }

            return $token;
        });
    }

    /**
     * 刷新 api token
     */
    public function refreshApiToken()
    {
        $this->api_token = str_random(60);
    }

    /**
     * 生成交易单号
     * 取用户id后四位配合生成，便于数据存储扩展
     *
     * @return string
     */
    public function randomTradeNo()
    {
        // 取user_id的后四位
        $mod = $this->id % 10000;
        $suffix = str_pad($mod, 4, '0', STR_PAD_LEFT);
        return date('Ymd') . number_random(8) . $suffix;
    }
}

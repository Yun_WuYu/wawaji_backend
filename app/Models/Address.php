<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 收货地址
 *
 * Class Address
 * @package App\Models
 *
 * @property Region $province 省份
 * @property Region $city 城市
 * @property Region $district 城区
 *
 * @property int $id
 * @property int $user_id 用户id
 * @property string $address 详细地址
 * @property string $consignee 收货人
 * @property string $phone 手机号
 * @property int $is_default 是否为默认地址
 */
class Address extends Model
{
    protected $guarded = [];
    
    /**
     * 省份
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Region::class, 'province_id');
    }
    
    /**
     * 城市
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(Region::class, 'city_id');
    }
    
    /**
     * 城区
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo(Region::class, 'district_id');
    }
    
    /**
     * 完整地址
     *
     * @return string
     */
    public function fullAddress()
    {
        return $this->province->name . $this->city->name . $this->district->name .
            $this->address;
    }
}

<?php

namespace App\Models;

use App\Support\Traits\DisableUpdatedAt;
use Illuminate\Database\Eloquent\Model;

/**
 * 游戏币记录
 *
 * Class CoinRecord
 * @package App\Models
 *
 * @property \App\Models\User $user 用户
 *
 * @property int $coin 游戏币个数（正负值）
 * @property string $recordable_type 记录类型
 * @property \Carbon\Carbon $created_at
 */
class CoinRecord extends Model
{
    use DisableUpdatedAt;

    /**
     * 类型
     */
    const TYPE_GAME_RECORD = 'game_record';        // 游戏消费
    const TYPE_PAYMENT = 'payment';                // 购买
    const TYPE_PAYMENT_AWARD = 'payment_award';    // 充值赠送
    const TYPE_REGISTER = 'register';              // 注册赠送
    const TYPE_SYSTEM = 'system';                  // 系统赠送

    protected $guarded = [];

    /**
     * 记录类型
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function recordable()
    {
        return $this->morphTo();
    }
    
    /**
     * 用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

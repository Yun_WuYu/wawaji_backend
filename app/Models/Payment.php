<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 支付
 *
 * Class Payment
 * @package App\Models
 *
 * @property User $user 用户
 *
 * @property int $id 支付id
 * @property int $user_id 用户id
 * @property string $status 状态：unpaid, paid
 * @property string $trade_no 交易单号
 * @property int $coin 当次支付得到的游戏币个数（购买的+赠送的）
 * @property float $money 支付金额
 * @property string $paid_at 支付时间
 * @property \Carbon\Carbon $created_at 创建时间
 */
class Payment extends Model
{
    const STATUS_UNPAID = 'unpaid';
    const STATUS_PAID = 'paid';

    const TYPE_COIN = 'coin';

    protected $guarded = [];

    /**
     * 记录类型
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function payable()
    {
        return $this->morphTo();
    }

    /**
     * 用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    /**
     * 是否已支付
     *
     * @return bool
     */
    public function isPaid()
    {
        return self::STATUS_PAID == $this->status;
    }
}

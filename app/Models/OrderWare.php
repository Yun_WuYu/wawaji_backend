<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 订单商品
 *
 * Class OrderWare
 * @package App\Models
 */
class OrderWare extends Model
{
    public $timestamps = false;

    protected $guarded = [];
}

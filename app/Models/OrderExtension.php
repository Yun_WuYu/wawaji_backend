<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 订单扩展
 *
 * Class OrderExtension
 * @package App\Models
 *
 * @property int $province_id 省份id
 * @property int $city_id 城市id
 * @property int $district_id 城区id
 */
class OrderExtension extends Model
{
    protected $primaryKey = 'order_id';
    
    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    /**
     * 省市区
     *
     * @return array
     */
    public function regions()
    {
        return [
            'province' => Region::find($this->province_id)->name,
            'city' => Region::find($this->city_id)->name,
            'district' => Region::find($this->district_id)->name,
        ];
    }
}

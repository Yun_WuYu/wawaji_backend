<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 订单
 *
 * Class Order
 * @package App\Models
 *
 * @property OrderExtension $extension 订单扩展信息
 *
 * @property int $user_id 用户id
 */
class Order extends Model
{
    use SoftDeletes;

    const STATUS_WAIT = 100;
    const STATUS_SHIPPED = 200;
    const STATUS_DONE = 1000;

    const SEND_TYPE_SELF = 1;
    const SEND_TYPE_SUPPLIER = 2;

    protected $guarded = [];

    /**
     * 订单商品信息
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function wares()
    {
        return $this->hasMany(OrderWare::class);
    }

    /**
     * 订单扩展信息
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function extension()
    {
        return $this->hasOne(OrderExtension::class);
    }
}

<?php

namespace App\Models;

use App\Support\Traits\DisableUpdatedAt;
use Illuminate\Database\Eloquent\Model;

/**
 * 第三方登录用户
 *
 * Class ThirdPartyUser
 * @package App\Models
 *
 * @property User $user 用户
 */
class ThirdPartyUser extends Model
{
    use DisableUpdatedAt;
    
    /**
     * 第三方登录类型
     */
    const TYPE_WX_OFFICIAL = 1;    // 微信公众号
    const TYPE_WX_MINI = 2;        // 微信小程序
    const TYPE_WX_MOBILE = 3;      // 微信移动应用
    
    protected $guarded = [];
    
    /**
     * 所属用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

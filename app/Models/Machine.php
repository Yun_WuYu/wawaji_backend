<?php

namespace App\Models;

use App\Common\CacheKey;
use App\Services\QCloud\QCloud;
use App\Support\Time;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * 游戏机
 *
 * Class Machine
 * @package App\Models
 *
 * @property \Illuminate\Database\Eloquent\Collection $cameras 摄像头
 * @property \App\Models\User $latestUser 最近一次的玩家
 *
 * @property int $id
 * @property string $name 设备名称
 * @property string $ware_name 商品名称
 * @property string $device_id 设备号
 * @property string $room_id 房间号
 * @property int $device_status 状态：0为空闲，1为游戏中，2为故障
 * @property int $is_display 是否显示：0为否，1为是
 * @property int $coin 1次消耗游戏币个数
 * @property string $ware_image 商品图片
 * @property int $latest_user_id 最近一次玩家的用户id
 */
class Machine extends Model
{
    /**
     * 设备状态
     */
    const DEVICE_STATUS_IDLE = 0; // 空闲
    const DEVICE_STATUS_WORKING = 1; // 工作中
    const DEVICE_STATUS_FAULT = 2; // 故障

    protected $guarded = [];
    
    /**
     * 最近一次的玩家
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function latestUser()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 摄像头
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cameras()
    {
        return $this->hasMany(Camera::class);
    }

    /**
     * 游戏记录
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gameRecords()
    {
        return $this->hasMany(GameRecord::class);
    }
    
    /**
     * 是否在工作中
     *
     * @return bool
     */
    public function isWorking()
    {
        return self::DEVICE_STATUS_WORKING == $this->device_status;
    }
    
    /**
     * 是否可用
     *
     * @return bool
     */
    public function enable()
    {
        return self::DEVICE_STATUS_FAULT != $this->device_status && 1 == $this->is_display;
    }
    
    /**
     * 获取腾讯云的 sig
     *
     * @return string
     */
    public function getQCloudSig()
    {
        $key = CacheKey::machineQCloudSig($this->id);
        
        // 缓存 9 天
        return Cache::remember($key, 9 * Time::MINUTES_OF_DAY, function () {
            // 过期时间为 10 天
            $expired = 10 * Time::SECONDS_OF_DAY;
            
            $qcloud = new QCloud();
            return $qcloud->genRoomSig($this->room_id, $expired);
        });
    }
}

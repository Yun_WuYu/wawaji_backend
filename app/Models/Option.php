<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 配置项
 *
 * Class Option
 * @package App\Models
 *
 * @property string $value 配置值
 */
class Option extends Model
{
    public $timestamps = false;
    
    protected $guarded = [];
    
    /**
     * 获取配置值
     *
     * @param mixed $value
     * @return mixed
     */
    public function getValueAttribute($value)
    {
        return maybe_json_decode($value, true);
    }
    
    /**
     * 设置配置值
     *
     * @param $value
     * @return null
     */
    public function setValueAttribute($value)
    {
        if (is_null($value)) {
            $value = '';
        }
        
        $this->attributes['value'] = maybe_json_encode($value);
    }
    
    /**
     * 通过名称查找
     *
     * @param string $name
     * @return self|null
     */
    public static function findByName($name)
    {
        return self::where('name', $name)->first();
    }
}

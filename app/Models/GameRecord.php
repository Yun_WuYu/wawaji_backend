<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 游戏记录
 *
 * Class GameRecord
 * @package App\Models
 *
 * @property User $user 用户
 *
 * @property int $id
 * @property int $user_id 用户id
 * @property int $operate_result 操作结果：-1为未有结果，1为成功，2为失败
 * @property \Carbon\Carbon $created_at
 */
class GameRecord extends Model
{
    /**
     * 操作结果
     */
    const OPERATE_ING = -1;
    const OPERATE_SUCCESS = 1;
    const OPERATE_FAILED = 2;
    
    /**
     * 商品状态
     */
    const WARE_STATUS_NONE = 0;        // 未抓中
    const WARE_STATUS_RECEIVABLE = 1;  // 可领取
    const WARE_STATUS_RECEIVED = 2;    // 已领取

    protected $guarded = [];

    /**
     * 记录用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 记录设备
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function machine()
    {
        return $this->belongsTo(Machine::class);
    }
}

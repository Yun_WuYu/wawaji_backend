<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 省市区
 *
 * Class Region
 * @package App\Models
 *
 * @property int $id
 * @property string $name 名称
 */
class Region extends Model
{
    const LEVEL_PROVINCE = 1;
    const LEVEL_CITY = 2;
    const LEVEL_DISTRICT = 3;

    public $timestamps = false;
    
    protected $guarded = [];
    
    /**
     * 上级地区
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }
    
    /**
     * 下属区域列表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}

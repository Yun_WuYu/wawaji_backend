<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 摄像头
 *
 * Class Camera
 * @package App\Models
 *
 * @property int $id
 * @property int $machine_id 游戏机id
 * @property string $type 类型：front为正面，side为侧面
 * @property string $live_openid 直播openid
 */
class Camera extends Model
{
    /**
     * 类型
     */
    const TYPE_FRONT = 'front';  // 正面
    const TYPE_SIDE = 'side';    // 侧面
    
    public $timestamps = false;
    
    protected $guarded = [];
}

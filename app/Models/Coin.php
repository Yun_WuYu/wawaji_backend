<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 游戏币
 *
 * Class Coin
 * @package App\Models
 *
 * @property int $id
 * @property int $coin 游戏币个数
 * @property int $coin_award 赠送游戏币个数
 * @property float $money 购买金额
 * @property int $status 状态：-1为不可用，1为可用
 * @property int $stick_time 置顶时间：0未置顶，其他值则已置顶
 */
class Coin extends Model
{
    /**
     * 状态
     */
    const STATUS_DISABLE = -1; // 不可用
    const STATUS_ENABLE = 1;   // 可用

    protected $guarded = [];
    
    /**
     * 是否可用
     *
     * @return bool
     */
    public function enable()
    {
        return self::STATUS_ENABLE == $this->status;
    }
}

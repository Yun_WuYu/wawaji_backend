<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Banner 图
 *
 * Class Banner
 * @package App\Models
 *
 * @property int $id
 * @property int $is_display 是否显示：0为否，1为是
 * @property string $image 图片
 */
class Banner extends Model
{
    protected $guarded = [];
}

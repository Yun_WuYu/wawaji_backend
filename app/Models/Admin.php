<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * 管理员用户
 *
 * Class Admin
 * @package App\Models
 *
 * @property int $id 用户id
 * @property string $username 用户名
 * @property string $api_token
 */
class Admin extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token',
    ];
    
    /**
     * 刷新 api token
     */
    public function refreshApiToken()
    {
        $this->api_token = str_random(60);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// 游戏围观
Broadcast::channel('machines.{machine}.spectators', function ($user) {
    return ['id' => $user->id];
});

// 游戏玩家排队
Broadcast::channel('machines.{machine}.player_queue', function ($user) {
    return ['id' => $user->id];
});

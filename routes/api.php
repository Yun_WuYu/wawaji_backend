<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// api在线文档
Route::get('doc/{admin?}', "DocController@index");

// 用户
Route::get('account', 'AccountController@show');
Route::post('account/login', 'AccountController@login');

Route::get('addresses/regions', 'AddressesController@regions');

// 轮播图
Route::get('banners', 'OpenController@banners');

// 游戏机硬件系统游戏结果回调
Route::get('callback/game-records', 'GameRecordsController@update');

// 游戏机硬件系统设备状态回调
Route::get('callback/machines', 'MachinesController@update');

// 游戏机硬件系统订单回调
Route::get('callback/orders', 'OrdersController@update');

// 微信支付回调
Route::post('callback/payments/{trade_type}', 'PaymentsController@update');

// 充值列表
Route::get('coins', 'PaymentsController@coins');

// 配置
Route::get('config', 'OpenController@config');

// 首页抓中展示
Route::get('doll-records/latest', 'OpenController@dollRecords');

// 游戏机游戏记录
Route::get('machines/{machine}/game-records', 'MachinesController@gameRecords');

// 游戏机列表、详情
Route::resource('machines', 'MachinesController', ['except' => [
    'store', 'update', 'destroy',
]]);

Route::get('wechat/js', 'WeChatController@js');
Route::get('wechat/oauth', 'WeChatController@oauth');
Route::post('wechat/oauth-mobile', 'WeChatController@oauthMobile');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('account/logout', 'AccountController@logout');

    Route::get('addresses/default', 'AddressesController@defaultAddress');
    Route::resource('addresses', 'AddressesController');

    // 娃娃币记录
    Route::get('coin-records', 'PaymentsController@coinRecords');
    
    // 我的娃娃记录
    Route::get('doll-records', 'GameRecordsController@dollRecords');
    
    // 游戏操控
    Route::get('game-records/doll-operate', 'GameRecordsController@dollOperate');
    
    // 游戏记录
    Route::resource('game-records', 'GameRecordsController');
    
    // 进入房间
    Route::post('machines/{machine}/room', 'MachinesController@room');

    // 确认收货
    Route::put('orders/{order}/confirm', 'OrdersController@confirm');

    // 物流查询
    Route::get('orders/{order}/logistics', 'OrdersController@logistics');

    // 订单
    Route::resource('orders', 'OrdersController');

    // 模拟购买游戏币
    Route::post('payments/fake-pay', 'PaymentsController@fakePay');
    
    // 获取支付配置参数
    Route::get('payments/{payment}/prepay-params', 'PaymentsController@prepayParams');
    
    // 交易单号创建、查询
    Route::resource('payments', 'PaymentsController');
});


/* ----------------------------------- 管理后台 ------------------------------------ */

// 账号信息
Route::get('admin/account', 'Admin\AccountController@show');

// 登录
Route::post('admin/account/login', 'Admin\AccountController@login');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
    // 退出
    Route::post('account/logout', 'AccountController@logout');
    // 重设密码
    Route::post('account/reset', 'AccountController@resetPassword');

    // 轮播图列表
    Route::get('banners', 'BannersController@index');

    // 轮播图创建
    Route::post('banners', 'BannersController@store');

    // 轮播图更新（上传文件时，需要使用 post）
    Route::post('banners/{banner}', 'BannersController@update');

    // 充值项列表
    Route::get('coins', 'CoinsController@index');

    // 充值项更新
    Route::put('coins/{coin}', 'CoinsController@update');
    
    // 游戏币记录
    Route::get('coin-records', 'CoinRecordsController@index');
    Route::post('coin-records', 'CoinRecordsController@store');
    
    // 游戏币统计
    Route::get('coin-records/statistics', 'CoinRecordsController@statistics');
    
    // 游戏记录
    Route::get('game-records', 'GameRecordsController@index');

    // 同步设备
    Route::post('machines/sync', 'MachinesController@sync');

    // 游戏机列表
    Route::get('machines', 'MachinesController@index');

    // 游戏机更新
    Route::post('machines/{machine}', 'MachinesController@update');

    // 游戏机详情
    Route::get('machines/{machine}', 'MachinesController@show');

    // 配置信息
    Route::get('options', 'OptionsController@index');
    
    // 更新配置信息
    Route::post('options', 'OptionsController@store');
    
    // 订单列表
    Route::get('orders', 'OrdersController@index');
    
    // 订单详情
    Route::get('orders/{id}', 'OrdersController@show');

    // 充值列表
    Route::get('payments', 'PaymentsController@index');

    // 支付统计
    Route::get('payments/recharge', 'PaymentsController@recharge');

    // 用户管理列表
    Route::get('users', 'UsersController@index');
});
